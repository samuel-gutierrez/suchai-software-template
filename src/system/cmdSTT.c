#include "cmdSTT.h"

static const char *tag = "cmdSTT";


#define TM_TYPE_STT 30
#define SCH_TRX_PORT_STT 17

void cmd_stt_init(void)
{
    cmd_add("stt_test", stt_test, "%d", 1);
    cmd_add("stt_reboot", stt_reboot, "%d", 1);
    cmd_add("stt_test_mux", stt_test_mux, "%d %s", 2);
    cmd_add("stt_take_one_pic", stt_take_one_pic, "%d %s", 2);
    cmd_add("stt_take_pic_no_exp", stt_take_pic_no_exp, "%d", 1);
    cmd_add("stt_evaluate_pic", stt_evaluate_pic, "%d %s", 2);
    cmd_add("stt_exp_time_evaluation", stt_exp_time_evaluation, "%d %s %s %s %s", 5);
    cmd_add("stt_gyro", stt_gyro, "%d", 1);
    cmd_add("stt_solve_lis_one_pic", stt_solve_lis_one_pic, "%d %s %s", 3);
    cmd_add("stt_solve_lis_and_track", stt_solve_lis_and_track, "%d %s %s %s", 4);
    cmd_add("stt_take_pic_flir", stt_take_pic_flir, "%d", 1);
    cmd_add("stt_led_on", stt_led_on, "%d %s", 2);
    cmd_add("stt_chamber_pic", stt_chamber_pic, "%d", 1);
    cmd_add("stt_rpi_temp", stt_rpi_temp, "%d", 1);
    cmd_add("stt_ls_last_files", stt_ls_last_files, "%d %s %s", 3);
    cmd_add("stt_erase_data", stt_erase_data, "%d %s", 2);
    cmd_add("stt_get_folder_size", stt_get_folder_size, "%d %s", 2);
    cmd_add("stt_divide_img", stt_divide_img, "%d %s %s %s", 4);
}

int stt_test(char* fmt, char* params, int nparams)
{
    /* Test if the Raspberry Pi is turned on and correctly initialized */
    int node;

    if(params == NULL || sscanf(params, fmt, &node) != nparams)
        return CMD_SYNTAX_ERROR;

    LOGI(tag, "--> STT: OK!");
    char buff[10] = "STT OK";
    int rc = com_send_debug(node, buff, strlen(buff));
    return CMD_OK;
}

int stt_reboot(char* fmt, char* params, int nparams)
{
    /* Reboot the Raspberry Pi */
    int node;

    if(params == NULL || sscanf(params, fmt, &node) != nparams)
        return CMD_SYNTAX_ERROR;

    LOGI(tag,"--> STT: Rebooting...");
    system("sudo reboot");
    return CMD_OK;
}

int stt_test_mux(char* fmt, char* params, int nparams)
{
    /* Test if the camera multiplexer is working properly */
    int node;
    char cam_nmbr[SCH_CMD_MAX_STR_PARAMS];
    char task[SCH_CMD_MAX_STR_PARAMS];

    if(params == NULL || sscanf(params, fmt, &node, cam_nmbr) != nparams)
        return CMD_SYNTAX_ERROR;

    snprintf(task, sizeof(task), "python3 /home/pi/suchai-software-template/stt_python/test_mux.py %s", cam_nmbr);

    int system_return = system(task);
    int status = WEXITSTATUS(system_return);
    int rc;

    if (status == 77) {
        LOGI(tag, "RESULT: CAM1 OK");
        char buff[10] = "CAM1 OK";
        rc = com_send_debug(node, buff, strlen(buff));
        return CMD_OK;
    }
    else if (status == 78) {
        LOGI(tag, "RESULT: CAM2 OK");
        char buff[10] = "CAM2 OK";
        rc = com_send_debug(node, buff, strlen(buff));
        return CMD_OK;
    }
    else if (status == 79) {
        LOGE(tag, "RESULT: SCRIPT ERROR");
        char buff[20] = "SCRIPT ERROR";
        rc = com_send_debug(node, buff, strlen(buff));
        return CMD_OK;
    }
    else {
        LOGE(tag, "RESULT: ANOTHER KIND OF ERROR");
        char buff[20] = "OTHER ERROR";
        rc = com_send_debug(node, buff, strlen(buff));
        return CMD_ERROR;
    }
}

int stt_take_one_pic(char* fmt, char* params, int nparams)
{
    /* Take one pic with the STT camera, setting the exposure time */
    int node;
    char exp_time[SCH_CMD_MAX_STR_PARAMS];
    char task[SCH_CMD_MAX_STR_PARAMS];

    if(params == NULL || sscanf(params, fmt, &node, exp_time) != nparams)
        return CMD_SYNTAX_ERROR;

    snprintf(task, sizeof(task), "python3 /home/pi/suchai-software-template/stt_python/take_one_picture.py %s", exp_time);

    int system_return = system(task);
    int status = WEXITSTATUS(system_return);
    int rc;

    if (status == 77) {
        LOGI(tag, "RESULT: OK");
        char buff[10] = "OK";
        rc = com_send_debug(node, buff, strlen(buff));
        return CMD_OK;
    }
    if (status == 78) {
        LOGE(tag, "RESULT: SCRIPT ERROR");
        char buff[20] = "SCRIPT ERROR";
        rc = com_send_debug(node, buff, strlen(buff));
        return CMD_OK;
    }
    else {
        LOGE(tag, "RESULT: ANOTHER KIND OF ERROR");
        char buff[20] = "OTHER ERROR";
        rc = com_send_debug(node, buff, strlen(buff));
        return CMD_ERROR;
    }
}

int stt_take_pic_no_exp(char* fmt, char* params, int nparams)
{
    /* Take one pic with the STT camera without setting the exposure time  */
    int node;

    if(params == NULL || sscanf(params, fmt, &node) != nparams)
        return CMD_SYNTAX_ERROR;

    int system_return = system("python3 /home/pi/suchai-software-template/stt_python/take_one_pic_no_exposure.py");
    int status = WEXITSTATUS(system_return);
    int rc;

    if (status == 77) {
        LOGI(tag, "RESULT: OK");
        char buff[10] = "OK";
        rc = com_send_debug(node, buff, strlen(buff));
        return CMD_OK;
    }
    if (status == 78) {
        LOGE(tag, "RESULT: SCRIPT ERROR");
        char buff[20] = "SCRIPT ERROR";
        rc = com_send_debug(node, buff, strlen(buff));
        return CMD_OK;
    }
    else {
        LOGE(tag, "RESULT: ANOTHER KIND OF ERROR");
        char buff[20] = "OTHER ERROR";
        rc = com_send_debug(node, buff, strlen(buff));
        return CMD_ERROR;
    }
}

int stt_evaluate_pic(char* fmt, char* params, int nparams)
{
    /* Take one pic with the STT camera, setting the exposure time, and evaluate if there are stars in it */
    int node;
    char exp_time[SCH_CMD_MAX_STR_PARAMS];
    char task[SCH_CMD_MAX_STR_PARAMS];

    if(params == NULL || sscanf(params, fmt, &node, exp_time) != nparams)
        return CMD_SYNTAX_ERROR;

    // snprintf(task, sizeof(task), "python3 /home/samuel/GitLab/suchai-software-template/stt_python/take_one_pic_and_eval.py %s", exp_time);
    snprintf(task, sizeof(task), "python3 /home/pi/suchai-software-template/stt_python/take_one_pic_and_eval.py %s", exp_time);
    // printf("-> TASK: %s \n", task);

    int system_return = system(task);
    int status = WEXITSTATUS(system_return);
    int rc;
    // printf("-> system return: %d \n", system_return);
    // printf("-> WEXITSTATUS: %d\n", WEXITSTATUS(system_return));

    if (status == 77) {
        LOGI(tag, "RESULT: EVAL ERROR");
        char buff[10] = "EVAL ERROR";
        rc = com_send_debug(node, buff, strlen(buff));
        return CMD_OK;
    }
    else if (status == 76) {
        LOGI(tag, "RESULT: EVAL OK");
        char buff[10] = "EVAL OK";
        rc = com_send_debug(node, buff, strlen(buff));
        return CMD_OK;
    }
    else if (status == 78) {
        LOGE(tag, "RESULT: SCRIPT ERROR");
        char buff[20] = "SCRIPT ERROR";
        rc = com_send_debug(node, buff, strlen(buff));
        return CMD_ERROR;
    }
    else {
        LOGE(tag, "RESULT: ANOTHER KIND OF ERROR");
        char buff[20] = "OTHER ERROR";
        rc = com_send_debug(node, buff, strlen(buff));
        return CMD_ERROR;
    }
}

int stt_exp_time_evaluation(char* fmt, char* params, int nparams)
{
    /* Experiment to evaluate the exposure time in which a specific number of stars are extracted */
    if(params == NULL) {
        LOGE(tag, "Null arguments!");
        return CMD_SYNTAX_ERROR;
    }

    int node;
    char init_time[SCH_CMD_MAX_STR_PARAMS];
    char final_time[SCH_CMD_MAX_STR_PARAMS];
    char time_step[SCH_CMD_MAX_STR_PARAMS];
    char n_stars[SCH_CMD_MAX_STR_PARAMS];

    char task[SCH_CMD_MAX_STR_PARAMS];
    if(sscanf(params, fmt, &node, init_time, final_time, time_step, n_stars) == nparams) {
        char *filename = "/home/pi/suchai-software-template/stt_data/exp_time.txt"; // Path to file with data
        // char *filename = "/home/samuel/GitLab/suchai-software-template/stt_data/exp_time.txt"; // Path to file with data

        snprintf(task, sizeof(task), "python3 /home/pi/suchai-software-template/stt_python/evaluate_exp_time.py %s %s %s %s", init_time, final_time, time_step, n_stars);
        // snprintf(task, sizeof(task), "python3 /home/samuel/GitLab/suchai-software-template/stt_python/evaluate_exp_time.py %s %s %s %s", init_time, final_time, time_step, n_stars);
        int system_return = system(task); // Call script
        int status = WEXITSTATUS(system_return);

        if (status == 77) {
            FILE *myfile;
            // Create structure, read file and save values
            stt_exp_time_data_t time_data;
            myfile = fopen(filename, "r");

            if (fscanf(myfile, "%d %d", &time_data.exp_time, &time_data.n_stars) == 2) {
                fclose(myfile);
                // printf("%d %d", time_data.exp_time, time_data.n_stars); //For debugging purposes

                int index_stt = dat_get_system_var(data_map[stt_exp_time_sensors].sys_index);
                int curr_time =  (int)time(NULL);
                time_data.index = index_stt;
                time_data.timestamp = curr_time;
                dat_add_payload_sample(&time_data, stt_exp_time_sensors);
                return CMD_OK;
            }
            else {
                fclose(myfile);
                LOGE(tag, "Error parsing data")
                return CMD_ERROR;
            }
        }
        else if (status == 78) {
            LOGE(tag, "RESULT: SCRIPT ERROR");
            return CMD_ERROR;
        }
        else {
            LOGE(tag, "RESULT: ANOTHER KIND OF ERROR");
            return CMD_ERROR;
        }
    }
    else {
        LOGE(tag, "Error parsing parameters!");
        return CMD_SYNTAX_ERROR;
    }
}

int stt_gyro(char* fmt, char* params, int nparams)
{
    /* Take data from the gyroscope */
    int node;

    if(params == NULL || sscanf(params, fmt, &node) != nparams)
        return CMD_SYNTAX_ERROR;

    char *filename = "/home/pi/suchai-software-template/stt_data/gyro_data.txt";

    int system_return = system("python3 /home/pi/suchai-software-template/stt_python/data_from_gyro.py");
    int status = WEXITSTATUS(system_return);

    if (status == 77) {
        FILE *myfile;
        // Create structure, read file and save values
        stt_gyro_data_t gyro_data;
        myfile = fopen(filename, "r");
        if (fscanf(myfile, "%f %f %f", &gyro_data.gx, &gyro_data.gy, &gyro_data.gz) == 3) {
            LOGR(tag, "%f %f %f", gyro_data.gx, gyro_data.gy, gyro_data.gz); //For debugging purposes

            int index_stt = dat_get_system_var(data_map[stt_gyro_sensors].sys_index);
            int curr_time =  (int)time(NULL);
            gyro_data.index = index_stt;
            gyro_data.timestamp = curr_time;
            dat_add_payload_sample(&gyro_data, stt_gyro_sensors);
            // if(node >= 0)   --- This is for send data
            //     com_send_telemetry(node, SCH_TRX_PORT_STT, TM_TYPE_STT, &gyro_data, sizeof(gyro_data), 1, 0);
            return CMD_OK;
        }
        else {
            fclose(myfile);
            LOGE(tag, "Error parsing data")
            return CMD_ERROR;
        }
    }
    else if (status == 78) {
        LOGE(tag, "RESULT: SCRIPT ERROR");
        return CMD_ERROR;
    }
    else {
        LOGE(tag, "RESULT: ANOTHER KIND OF ERROR");
        return CMD_ERROR;
    }
}

int stt_solve_lis_one_pic(char* fmt, char* params, int nparams)
{
    /* Take a picture with a specific exposure time, solve the LIS problem and save relevant data */
    if(params == NULL) {
        LOGW(tag, "Null arguments!");
        return CMD_SYNTAX_ERROR;
    }

    int node;
    char exp_time[SCH_CMD_MAX_STR_PARAMS];
    char cat_division[SCH_CMD_MAX_STR_PARAMS];

    char task[SCH_CMD_MAX_STR_PARAMS];

    if(sscanf(params, fmt, &node, exp_time, cat_division) == nparams) {
        char *filename = "/home/pi/suchai-software-template/stt_data/lis_one_image.txt"; // Path to file with data of the angle
        // char *filename = "/home/samuel/GitLab/suchai-software-template/stt_data/lis_one_image.txt"; // Path to file with data of the angle

        snprintf(task, sizeof(task), "python3 /home/pi/suchai-software-template/stt_python/solve_lis_one_image.py %s %s", exp_time, cat_division);
        // snprintf(task, sizeof(task), "python3 /home/samuel/GitLab/suchai-software-template/stt_python/solve_lis_one_image.py %s %s", exp_time, cat_division);
        int system_return = system(task); //Call script
        int status = WEXITSTATUS(system_return);

        if (status == 77) {
            FILE *myfile;
            // Create structure, read file and save values
            stt_data_t angle;
            // memset(&angle, 0, sizeof(angle));
            myfile = fopen(filename, "r");

            if (fscanf(myfile, "%f %f %f %d %f", &angle.ra, &angle.dec, &angle.roll, &angle.time, &angle.exec_time) == 5) {
                fclose(myfile);
                // printf("%f %f %f %d %f", angle.ra, angle.dec, angle.roll, angle.time, angle.exec_time); //For debugging purposes

                int index_stt = dat_get_system_var(data_map[stt_sensors].sys_index);
                int curr_time =  (int)time(NULL);
                stt_data_t  stt_data  = {index_stt, curr_time, angle.ra, angle.dec, angle.roll, angle.time, angle.exec_time};
                int ret = dat_add_payload_sample(&stt_data, stt_sensors);

                // Send angle data to node
                int rc = _com_send_data(node, (void *) &stt_data, sizeof(stt_data), TM_TYPE_PAYLOAD+stt_sensors, 1, 1);
                return CMD_OK;
            }
            else {
                fclose(myfile);
                LOGE(tag, "Error parsing data")
                return CMD_ERROR;
            }
        }
        else if (status == 78) {
            LOGE(tag, "RESULT: SCRIPT ERROR");
            char buff[20] = "SCRIPT ERROR";
            int rc = com_send_debug(node, buff, strlen(buff));
            return CMD_ERROR;
        }
        else {
            LOGE(tag, "RESULT: ANOTHER KIND OF ERROR");
            char buff[20] = "OTHER ERROR";
            int rc = com_send_debug(node, buff, strlen(buff));
            return CMD_ERROR;
        }
    }
    else {
        LOGE(tag, "Error parsing parameters!");
        return CMD_ERROR;
    }
}

int stt_solve_lis_and_track(char* fmt, char* params, int nparams)
{
    /* Take a picture with a specific exposure time, solve the LIS problem and keep track using previous data */
    int node;
    char exp_time[SCH_CMD_MAX_STR_PARAMS];
    char cat_division[SCH_CMD_MAX_STR_PARAMS];
    char n_track[SCH_CMD_MAX_STR_PARAMS];
    char task[SCH_CMD_MAX_STR_PARAMS];

    if(params == NULL || sscanf(params, fmt, &node, exp_time, cat_division, n_track) != nparams)
        return CMD_SYNTAX_ERROR;

    snprintf(task, sizeof(task), "python3 /home/pi/suchai-software-template/stt_python/solve_lis_and_tracking.py %s %s %s", exp_time, cat_division, n_track);
    // snprintf(task, sizeof(task), "python3 /home/samuel/GitLab/suchai-software-template/stt_python/solve_lis_and_tracking.py %s %s %s", exp_time, cat_division, n_track);

    int system_return = system(task);
    int status = WEXITSTATUS(system_return);
    int rc;

    if (status == 77) {
        LOGI(tag, "RESULT: COMMAND EXECUTED");
        char buff[10] = "OK";
        rc = com_send_debug(node, buff, strlen(buff));
        return CMD_OK;
    }
    else if (status == 78) {
        LOGE(tag, "RESULT: SCRIPT ERROR");
        char buff[20] = "SCRIPT ERROR";
        rc = com_send_debug(node, buff, strlen(buff));
        return CMD_ERROR;
    }
    else {
        LOGE(tag, "RESULT: ANOTHER KIND OF ERROR");
        char buff[20] = "OTHER ERROR";
        rc = com_send_debug(node, buff, strlen(buff));
        return CMD_ERROR;
    }
    return CMD_OK;
}

int stt_take_pic_flir(char* fmt, char* params, int nparams)
{
    /* Take a picture with the FLIR camera */
    int node;

    if(params == NULL || sscanf(params, fmt, &node) != nparams)
        return CMD_SYNTAX_ERROR;

    int system_return = system("python3 /home/pi/suchai-software-template/stt_python/flir_picture.py");
    int status = WEXITSTATUS(system_return);
    int rc;

    if (status == 77) {
        LOGI(tag, "RESULT: OK");
        char buff[10] = "OK";
        rc = com_send_debug(node, buff, strlen(buff));
        return CMD_OK;
    }
    else if (status == 78) {
        LOGE(tag, "RESULT: SCRIPT ERROR");
        char buff[20] = "SCRIPT ERROR";
        rc = com_send_debug(node, buff, strlen(buff));
        return CMD_ERROR;
    }
    else {
        LOGE(tag, "RESULT: ANOTHER KIND OF ERROR");
        char buff[20] = "OTHER ERROR";
        rc = com_send_debug(node, buff, strlen(buff));
        return CMD_ERROR;
    }
}

int stt_led_on(char* fmt, char* params, int nparams)
{
    /* Test if the LEDs are turning on correctly */
    int node;
    char n_led[SCH_CMD_MAX_STR_PARAMS];
    char task[SCH_CMD_MAX_STR_PARAMS];

    if(params == NULL || sscanf(params, fmt, &node, n_led) != nparams)
        return CMD_SYNTAX_ERROR;

    snprintf(task, sizeof(task), "python3 /home/pi/suchai-software-template/stt_python/turn_on_led.py %s",n_led);

    int system_return = system(task);
    int status = WEXITSTATUS(system_return);
    int rc;

    if (status == 77) {
        LOGI(tag, "RESULT: OK");
        char buff[10] = "OK";
        rc = com_send_debug(node, buff, strlen(buff));
        return CMD_OK;
    }
    else if (status == 78) {
        LOGE(tag, "RESULT: SCRIPT ERROR");
        char buff[20] = "SCRIPT ERROR";
        rc = com_send_debug(node, buff, strlen(buff));
        return CMD_ERROR;
    }
    else {
        LOGE(tag, "RESULT: ANOTHER KIND OF ERROR");
        char buff[20] = "OTHER ERROR";
        rc = com_send_debug(node, buff, strlen(buff));
        return CMD_ERROR;
    }
}

int stt_chamber_pic(char* fmt, char* params, int nparams)
{
    /* Take a picture of the chamber (without setting the exposure time) */
    int node;

    if(params == NULL || sscanf(params, fmt, &node) != nparams)
        return CMD_SYNTAX_ERROR;

    int system_return = system("python3 /home/pi/suchai-software-template/stt_python/take_chamber_pic.py");
    int status = WEXITSTATUS(system_return);
    int rc;

    if (status == 77) {
        LOGI(tag, "RESULT: OK");
        char buff[10] = "OK";
        rc = com_send_debug(node, buff, strlen(buff));
        return CMD_OK;
    }
    else if (status == 78) {
        LOGE(tag, "RESULT: SCRIPT ERROR");
        char buff[20] = "SCRIPT ERROR";
        rc = com_send_debug(node, buff, strlen(buff));
        return CMD_ERROR;
    }
    else {
        LOGE(tag, "RESULT: ANOTHER KIND OF ERROR");
        char buff[20] = "OTHER ERROR";
        rc = com_send_debug(node, buff, strlen(buff));
        return CMD_ERROR;
    }
}

int stt_rpi_temp(char* fmt, char* params, int nparams)
{
    /* Measure the RPi-core temperature */
    int node;

    char *cmd = "vcgencmd measure_temp | egrep -o '[0-9]*\\.[0-9]*'";
    /* printf("%s \n", cmd); */
    char temp[SCH_CMD_MAX_STR_PARAMS];
    FILE *fp;

    if(params == NULL || sscanf(params, fmt, &node) != nparams)
        return CMD_ERROR;

    if ((fp = popen(cmd, "r")) == NULL) {
        LOGE(tag, "Error opening pipe!");
        char buff[10] = "ERROR";
        int rc = com_send_debug(node, buff, strlen(buff));
        return CMD_ERROR;
    }

    while (fgets(temp, SCH_CMD_MAX_STR_PARAMS, fp) != NULL) {
        LOGI(tag, "OUTPUT: %s", temp);
    }

    if(pclose(fp))  {
        LOGE(tag, "Command not found or exited with error status");
        char buff[10] = "ERROR";
        int rc = com_send_debug(node, buff, strlen(buff));
        return CMD_ERROR;
    }

    int rc = _com_send_data(node, temp, sizeof(temp), 30, 1, 1);
    return CMD_OK;
}

int stt_ls_last_files(char* fmt, char* params, int nparams)
{
    /* List a specific number of files from the selected folder */
    int node;
    char folder[SCH_CMD_MAX_STR_PARAMS];
    char n_files[SCH_CMD_MAX_STR_PARAMS];
    char task[SCH_CMD_MAX_STR_PARAMS];
    char data[SCH_CMD_MAX_STR_PARAMS];
    FILE *fp;

    if(params == NULL || sscanf(params, fmt, &node, folder, n_files) != nparams)
        return CMD_SYNTAX_ERROR;

    if (strncmp(folder, "stt", 3) == 0) {
        snprintf(task, sizeof(task), "ls -1t /home/pi/STT_pictures/ | head -%s", n_files);

        if ((fp = popen(task, "r")) == NULL) {
            LOGE(tag, "Error opening pipe!");
            char buff[10] = "ERROR";
            int rc = com_send_debug(node, buff, strlen(buff));
            return CMD_ERROR;
        }
        while (fgets(data, SCH_CMD_MAX_STR_PARAMS, fp) != NULL) {
            LOGI(tag, "OUTPUT: %s", data);
        }
        if(pclose(fp))  {
            LOGE(tag, "Command not found or exited with error status");
            char buff[10] = "ERROR";
            int rc = com_send_debug(node, buff, strlen(buff));
            return CMD_ERROR;
        }
        int rc = _com_send_data(node, data, sizeof(data), 30, 1, 1);
        return CMD_OK;
    }
    else if (strncmp(folder, "chamber", 3) == 0) {
        snprintf(task, sizeof(task), "ls -1t /home/pi/chamber_pictures/ | head -%s", n_files);

        if ((fp = popen(task, "r")) == NULL) {
            LOGE(tag, "Error opening pipe!");
            char buff[10] = "ERROR";
            int rc = com_send_debug(node, buff, strlen(buff));
            return CMD_ERROR;
        }
        while (fgets(data, SCH_CMD_MAX_STR_PARAMS, fp) != NULL) {
            LOGI(tag, "OUTPUT: %s", data);
        }
        if(pclose(fp))  {
            LOGE(tag, "Command not found or exited with error status");
            char buff[10] = "ERROR";
            int rc = com_send_debug(node, buff, strlen(buff));
            return CMD_ERROR;
        }
        int rc = _com_send_data(node, data, sizeof(data), 30, 1, 1);
        return CMD_OK;
    }
    else if (strncmp(folder, "flir", 3) == 0) {
        snprintf(task, sizeof(task), "ls -1t /home/pi/FLIR_pictures/ | head -%s", n_files);

        if ((fp = popen(task, "r")) == NULL) {
            LOGE(tag, "Error opening pipe!");
            char buff[10] = "ERROR";
            int rc = com_send_debug(node, buff, strlen(buff));
            return CMD_ERROR;
        }
        while (fgets(data, SCH_CMD_MAX_STR_PARAMS, fp) != NULL) {
            LOGI(tag, "OUTPUT: %s", data);
        }
        if(pclose(fp))  {
            LOGE(tag, "Command not found or exited with error status");
            char buff[10] = "ERROR";
            int rc = com_send_debug(node, buff, strlen(buff));
            return CMD_ERROR;
        }
        int rc = _com_send_data(node, data, sizeof(data), 30, 1, 1);
        return CMD_OK;
    }
    else if (strncmp(folder, "track", 3) == 0) {
        snprintf(task, sizeof(task), "ls -1t /home/pi/suchai-software-template/tracking_data/ | head -%s", n_files);

        if ((fp = popen(task, "r")) == NULL) {
            LOGE(tag, "Error opening pipe!");
            char buff[10] = "ERROR";
            int rc = com_send_debug(node, buff, strlen(buff));
            return CMD_ERROR;
        }
        while (fgets(data, SCH_CMD_MAX_STR_PARAMS, fp) != NULL) {
            LOGI(tag, "OUTPUT: %s", data);
        }
        if(pclose(fp))  {
            LOGE(tag, "Command not found or exited with error status");
            char buff[10] = "ERROR";
            int rc = com_send_debug(node, buff, strlen(buff));
            return CMD_ERROR;
        }
        int rc = _com_send_data(node, data, sizeof(data), 30, 1, 1);
        return CMD_OK;
    }
    else {
        char buff[10] = "ERROR";
        int rc = com_send_debug(node, buff, strlen(buff));
        return CMD_ERROR;
    }
}

int stt_erase_data(char* fmt, char* params, int nparams)
{
    /* Erase data from the selected folder */
    int node;
    char folder[SCH_CMD_MAX_STR_PARAMS];
    int rc;

    if(params == NULL || sscanf(params, fmt, &node, folder) != nparams)
        return CMD_SYNTAX_ERROR;

    if (strncmp(folder, "stt", 3) == 0) {
        system("rm -rf /home/pi/STT_pictures/*");
        char buff[10] = "OK";
        rc = com_send_debug(node, buff, strlen(buff));
        return CMD_OK;
    }
    else if (strncmp(folder, "chamber", 3) == 0) {
        system("rm -rf /home/pi/chamber_pictures/*");
        char buff[10] = "OK";
        rc = com_send_debug(node, buff, strlen(buff));
        return CMD_OK;
    }
    else if (strncmp(folder, "flir", 3) == 0) {
        system("rm -rf /home/pi/FLIR_pictures/*");
        char buff[10] = "OK";
        rc = com_send_debug(node, buff, strlen(buff));
        return CMD_OK;
    }
    else if (strncmp(folder, "track", 3) == 0) {
        system("rm -rf /home/pi/suchai-software-template/tracking_data/*");
        char buff[10] = "OK";
        rc = com_send_debug(node, buff, strlen(buff));
        return CMD_OK;
    }
    else {
        char buff[10] = "ERROR";
        rc = com_send_debug(node, buff, strlen(buff));
        return CMD_ERROR;
    }
}

int stt_get_folder_size(char* fmt, char* params, int nparams)
{
    /* Get the size of a selected folder */
    int node;
    char folder[SCH_CMD_MAX_STR_PARAMS];
    char size[SCH_CMD_MAX_STR_PARAMS];
    FILE *fp;

    if(params == NULL || sscanf(params, fmt, &node, folder) != nparams)
        return CMD_SYNTAX_ERROR;

    if (strncmp(folder, "stt", 3) == 0) {
        char *cmd = "du -h /home/pi/STT_pictures/ | cut -f1";

        if ((fp = popen(cmd, "r")) == NULL) {
            LOGE(tag, "Error opening pipe!");
            char buff[10] = "ERROR";
            int rc = com_send_debug(node, buff, strlen(buff));
            return CMD_ERROR;
        }
        while (fgets(size, SCH_CMD_MAX_STR_PARAMS, fp) != NULL) {
            LOGI(tag, "OUTPUT: %s", size);
        }
        if(pclose(fp))  {
            LOGE(tag, "Command not found or exited with error status");
            char buff[10] = "ERROR";
            int rc = com_send_debug(node, buff, strlen(buff));
            return CMD_ERROR;
        }
        int rc = _com_send_data(node, size, sizeof(size), 30, 1, 1);
        return CMD_OK;
    }
    else if (strncmp(folder, "chamber", 3) == 0) {
        char *cmd = "du -h /home/pi/chamber_pictures/ | cut -f1";

        if ((fp = popen(cmd, "r")) == NULL) {
            LOGE(tag, "Error opening pipe!");
            char buff[10] = "ERROR";
            int rc = com_send_debug(node, buff, strlen(buff));
            return CMD_ERROR;
        }
        while (fgets(size, SCH_CMD_MAX_STR_PARAMS, fp) != NULL) {
            LOGI(tag, "OUTPUT: %s", size);
        }
        if(pclose(fp))  {
            LOGE(tag, "Command not found or exited with error status");
            char buff[10] = "ERROR";
            int rc = com_send_debug(node, buff, strlen(buff));
            return CMD_ERROR;
        }
        int rc = _com_send_data(node, size, sizeof(size), 30, 1, 1);
        return CMD_OK;
     }
    else if (strncmp(folder, "flir", 3) == 0) {
        char *cmd = "du -h /home/pi/FLIR_pictures/ | cut -f1";

        if ((fp = popen(cmd, "r")) == NULL) {
            LOGE(tag, "Error opening pipe!");
            char buff[10] = "ERROR";
            int rc = com_send_debug(node, buff, strlen(buff));
            return CMD_ERROR;
        }
        while (fgets(size, SCH_CMD_MAX_STR_PARAMS, fp) != NULL) {
            LOGI(tag, "OUTPUT: %s", size);
        }
        if(pclose(fp))  {
            LOGE(tag, "Command not found or exited with error status");
            char buff[10] = "ERROR";
            int rc = com_send_debug(node, buff, strlen(buff));
            return CMD_ERROR;
        }
        int rc = _com_send_data(node, size, sizeof(size), 30, 1, 1);
        return CMD_OK;
    }
    else if (strncmp(folder, "track", 3) == 0) {
        char *cmd = "du -h /home/pi/suchai-software-template/tracking_data/ | cut -f1";

        if ((fp = popen(cmd, "r")) == NULL) {
            LOGE(tag, "Error opening pipe!");
            char buff[10] = "ERROR";
            int rc = com_send_debug(node, buff, strlen(buff));
            return CMD_ERROR;
        }
        while (fgets(size, SCH_CMD_MAX_STR_PARAMS, fp) != NULL) {
            LOGI(tag, "OUTPUT: %s", size);
        }
        if(pclose(fp))  {
            LOGE(tag, "Command not found or exited with error status");
            char buff[10] = "ERROR";
            int rc = com_send_debug(node, buff, strlen(buff));
            return CMD_ERROR;
        }
        int rc = _com_send_data(node, size, sizeof(size), 30, 1, 1);
        return CMD_OK;
    }
    else {
        char buff[10] = "ERROR";
        int rc = com_send_debug(node, buff, strlen(buff));
        return CMD_ERROR;
    }
}

int stt_divide_img(char* fmt, char* params, int nparams)
{
    /* Divide a specific image in chunks for easy download */
    int node;
    char folder[SCH_CMD_MAX_STR_PARAMS];
    char img_name[SCH_CMD_MAX_STR_PARAMS];
    char n_div[SCH_CMD_MAX_STR_PARAMS];

    if(params == NULL || sscanf(params, fmt, &node, folder, img_name, &n_div) != nparams)
        return CMD_SYNTAX_ERROR;

    char task[SCH_CMD_MAX_STR_PARAMS];
    snprintf(task, sizeof(task), "python3 /home/pi/suchai-software-template/stt_python/reducing_img_size.py %s %s %s", folder, img_name, n_div);

    int system_return = system(task);
    int status = WEXITSTATUS(system_return);
    int rc;

    if (status == 77) {
        LOGI(tag, "RESULT: OK");
        char buff[10] = "OK";
        rc = com_send_debug(node, buff, strlen(buff));
        return CMD_OK;
    }
    else if (status == 78) {
        LOGE(tag, "RESULT: SCRIPT ERROR");
        char buff[20] = "SCRIPT ERROR";
        rc = com_send_debug(node, buff, strlen(buff));
        return CMD_ERROR;
    }
    else {
        LOGE(tag, "RESULT: ANOTHER KIND OF ERROR");
        char buff[20] = "OTHER ERROR";
        rc = com_send_debug(node, buff, strlen(buff));
        return CMD_ERROR;
    }
}
