#include "globals.h"
#include "config.h"
#include "repoCommand.h"


void cmd_stt_init(void);
int stt_test(char* fmt, char* params, int nparams);
int stt_reboot(char* fmt, char* params, int nparams);
int stt_test_mux(char* fmt, char* params, int nparams);
int stt_take_one_pic(char* fmt, char* params, int nparams);
int stt_take_pic_no_exp(char* fmt, char* params, int nparams);
int stt_evaluate_pic(char* fmt, char* params, int nparams);
int stt_exp_time_evaluation(char* fmt, char* params, int nparams);
int stt_gyro(char* fmt, char* params, int nparams);
int stt_solve_lis_one_pic(char* fmt, char* params, int nparams);
int stt_solve_lis_and_track(char* fmt, char* params, int nparams);
int stt_take_pic_flir(char* fmt, char* params, int nparams);
int stt_led_on(char* fmt, char* params, int nparams);
int stt_chamber_pic(char* fmt, char* params, int nparams);
int stt_rpi_temp(char* fmt, char* params, int nparams);
int stt_ls_last_files(char* fmt, char* params, int nparams);
int stt_erase_data(char* fmt, char* params, int nparams);
int stt_get_folder_size(char* fmt, char* params, int nparams);
int stt_divide_img(char* fmt, char* params, int nparams);
