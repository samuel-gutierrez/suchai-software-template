import aux_functions as fn
import subprocess
import sys
import time

try:
    ep, f1p = [17, 4]
    fn.set_gpio(ep, f1p)

    exp_time = sys.argv[1]
    print("--> STT: Taking picture with {} ms of exposure time".format(exp_time))

    fn.select_camera(1, ep, f1p)
    time_ms = int(exp_time)
    time_micros = 1000 * time_ms
    current_time = time.strftime("%d%m%y_%H%M%S", time.localtime())
    pic_name = "/home/pi/STT_pictures/{}_{}ms.jpg".format(current_time, time_ms)
    task = 'raspistill -w 1024 -h 1024 -t 1 -ss {} -o {}'.format(time_micros, pic_name)
    print('EXECUTING: ', task)
    process = subprocess.Popen(task, shell=True, stdout=subprocess.PIPE)
    process.wait()
    fn.clean_gpio()
    sys.exit(77)
except Exception as err:
    print("--> ERROR:", err)
    fn.clean_gpio()
    sys.exit(78)
