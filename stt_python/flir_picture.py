import subprocess
import sys
import time

try:
    print("--> STT: Taking picture with FLIR camera")

    current_time = time.strftime("%d%m%y_%H%M%S", time.localtime())
    pic_name = "/home/pi/FLIR_pictures/{}.jpg".format(current_time)
    task = './home/pi/pylepton/pylepton_capture {}'.format(pic_name)
    print('EXECUTING: ', task)
    process = subprocess.Popen(task, shell=True, stdout=subprocess.PIPE)
    process.wait()
    sys.exit(77)
except Exception as err:
    print("--> ERROR:", err)
    sys.exit(78)
