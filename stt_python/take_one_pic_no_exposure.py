import aux_functions as fn
import subprocess
import sys
import time

try:
    ep, f1p = [17, 4]
    fn.set_gpio(ep, f1p)
    print("--> STT: Taking picture without setting exposure time")
    fn.select_camera(1, ep, f1p)

    current_time = time.strftime("%d%m%y_%H%M%S", time.localtime())
    pic_name = "/home/pi/STT_pictures/{}_noexp.jpg".format(current_time)
    task = 'raspistill -w 1024 -h 1024 -t 1 -o {}'.format(pic_name)
    print('EXECUTING: ', task)
    process = subprocess.Popen(task, shell=True, stdout=subprocess.PIPE)
    process.wait()
    fn.clean_gpio()
    sys.exit(77)
except Exception as err:
    print("--> ERROR:", err)
    fn.clean_gpio()
    sys.exit(78)
