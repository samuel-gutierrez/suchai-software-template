import aux_functions as fn
import stt_functions
import subprocess
import sys
import time

try:
    ep, f1p = [17, 4]
    fn.set_gpio(ep, f1p)

    exp_time = sys.argv[1]
    cat_division = int(sys.argv[2])
    print("--> STT: Taking picture with {} ms of exposure time".format(exp_time))
    fn.select_camera(1, ep, f1p)

    time_ms = int(exp_time)
    time_micros = 1000 * time_ms
    # current_time = time.strftime("%d%m%y_%H%M%S", time.localtime())
    current_time_to_database = int(time.time())
    current_time = str(current_time_to_database)
    pic_name = "/home/pi/STT_pictures/{}.jpg".format(current_time)
    task = 'raspistill -w 1024 -h 1024 -t 1 -ss {} -o {}'.format(time_micros, pic_name)
    print('EXECUTING: ', task)
    process = subprocess.Popen(task, shell=True, stdout=subprocess.PIPE)
    process.wait()

    # pic_name = '/home/pi/suchai-software-template/stt_data/sample_pic.jpg'
    # pic_name = '/home/samuel/GitLab/suchai-software-template/stt_data/sample_pic.jpg'
    stt_functions.solve_lis(pic_name, current_time_to_database, cat_division)
    fn.clean_gpio()
    sys.exit(77)
except Exception as err:
    print(err)
    # with open("/home/samuel/GitLab/suchai-software-template/stt_data/lis_one_image.txt", "w")as f:
    with open("/home/pi/suchai-software-template/stt_data/lis_one_image.txt", "w")as f:
        f.write("0 0 0 0 0")
    fn.clean_gpio()
    sys.exit(78)
