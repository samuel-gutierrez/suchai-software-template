import aux_functions as fn
import datetime as dt
import stt_functions
import subprocess
import sys
import time

try:
    ep, f1p = [17, 4]
    fn.set_gpio(ep, f1p)
    fn.select_camera(1, ep, f1p)

    exp_time = sys.argv[1]
    time_ms = int(exp_time)
    time_micros = 1000 * time_ms
    cat_division = int(sys.argv[2])
    n_track = sys.argv[3]
    print('--> N TRACK = {}'.format(n_track))

    init_time = dt.datetime.now().strftime("%y%m%d_%H%M%S")
    ra, dec, roll, nr = (0, 0, 0, 0)
    ii = 0

    while ii < int(n_track):
        print('--> TRACK INDEX = {}'.format(ii))
        print("--> STT tracking: Taking picture with {} ms of exposure time".format(exp_time))
        tm1 = time.time()
        pic_time = dt.datetime.now().strftime("%H%M%S_%f")[:-3]
        pic_name = "/home/pi/STT_pictures/{}_{}ms.jpg".format(pic_time, time_ms)
        # pic_name = "/home/samuel/GitLab/suchai-software-template/stt_data/sample_pic.jpg"
        # if ii == 5:
        #     pic_name = "/home/samuel/Desktop/japan.png"
        task = 'raspistill -w 1024 -h 1024 -t 1 -ss {} -o {}'.format(time_micros, pic_name)
        print('EXECUTING: ', task)
        process = subprocess.Popen(task, shell=True, stdout=subprocess.PIPE)
        process.wait()
        if ra == dec == roll == nr == 0:
            tm2a = time.time()
            ra, dec, roll, nr = stt_functions.solve_lis_problem(pic_name, cat_division, mod=True)
            tm3a = time.time()
            delta_t1a = tm2a - tm1
            delta_t2a = tm3a - tm2a
            if ra == dec == roll == nr == 0:
                str1a = '--> ERROR: Can not be capable to solve the LIS problem :(' \
                        ' Time_picture:{:.3f} Time_process:{:.3f}'.format(delta_t1a, delta_t2a)
                print(str1a)
                str11a = '{} RA:0.0 DEC:0.0 Roll:0.0 Nr:0 Time_picture:{:.3f} Time_process:{:.3f} NO_SOL_LIS\n'.\
                    format(pic_time, delta_t1a, delta_t2a)
                stt_functions.save_attitude_data(init_time, str11a)
            else:
                str2a = '{} RA:{:.3f} DEC:{:.3f} Roll:{:.3f} Nr:{} Time_picture:{:.3f} Time_process:{:.3f} LIS\n'. \
                    format(pic_time, ra, dec, roll, nr, delta_t1a, delta_t2a)
                print(str2a)
                stt_functions.save_attitude_data(init_time, str2a)
        else:
            tm2b = time.time()
            ra, dec, roll, nr = stt_functions.solve_tracking_problem(pic_name, ra, dec)
            tm3b = time.time()
            delta_t1b = tm2b - tm1
            delta_t2b = tm3b - tm2b
            if ra == dec == roll == nr == 0:
                str1b = '--> ERROR: Can not be capable to solve the tracking problem >:(' \
                        ' Time_picture:{:.3f} Time_process:{:.3f}'.format(delta_t1b, delta_t2b)
                print(str1b)
                str11b = '{} RA:0.0 DEC:0.0 Roll:0.0 Nr:0 Time_picture:{:.3f} Time_process:{:.3f} NO_SOL_TRACKING\n'.\
                    format(pic_time, delta_t1b, delta_t2b)
                stt_functions.save_attitude_data(init_time, str11b)
            else:
                str2b = '{} RA:{:.3f} DEC:{:.3f} Roll:{:.3f} Nr:{} Time_picture:{:.3f} Time_process:{:.3f} TRACKING\n'. \
                    format(pic_time, ra, dec, roll, nr, delta_t1b, delta_t2b)
                print(str2b)
                stt_functions.save_attitude_data(init_time, str2b)
        ii += 1
    fn.clean_gpio()
    sys.exit(77)
except Exception as err:
    print(err)
    fn.clean_gpio()
    sys.exit(78)
