import base64
import numpy as np
import os
import RPi.GPIO as gp
import zlib
from PIL import Image

DIR_CHUNK = '/home/pi/img_chunk'


def set_gpio(ep, f1p):
    gp.setwarnings(True)
    gp.setmode(gp.BCM)
    gp.setup(ep, gp.OUT)
    gp.setup(f1p, gp.OUT)
    return 0


def set_gpio_chamber(led1, led2, led3, led4, ep, f1p):
    gp.setwarnings(True)
    gp.setmode(gp.BOARD)
    gp.setup(led1, gp.OUT)
    gp.setup(led2, gp.OUT)
    gp.setup(led3, gp.OUT)
    gp.setup(led4, gp.OUT)
    gp.setup(ep, gp.OUT)
    gp.setup(f1p, gp.OUT)
    return 0


def select_camera(cam, ep, f1p):
    if cam == 1:
        os.system("i2cset -y 1 0x70 0x00 0x01")
        gp.output(ep, False)
        gp.output(f1p, False)
    elif cam == 2:
        os.system("i2cset -y 1 0x70 0x00 0x02")
        gp.output(ep, False)
        gp.output(f1p, True)
    else:
        raise NameError('--> ERROR: Please select a valid camera number!')
    return 0


def turn_on_led(led1, led2, led3, led4):
    gp.output(led1, True)
    gp.output(led2, True)
    gp.output(led3, True)
    gp.output(led4, True)
    return 0


def turn_off_led(led1, led2, led3, led4):
    gp.output(led1, False)
    gp.output(led2, False)
    gp.output(led3, False)
    gp.output(led4, False)
    return 0


def clean_gpio():
    gp.cleanup()
    return 0


def reduce_img_size(pic_name):
    img = Image.open(pic_name)
    img_width, img_height = img.size
    img = img.resize((img_width//10, img_height//10), Image.ANTIALIAS)
    img.save(pic_name, optimize=True, quality=95)
    return 0


def img2str(dir_img):
    """
    Transform an image to str.
    """
    with open(dir_img, "rb") as image2string:
        converted_string = base64.b64encode(image2string.read())
    return converted_string


def compress_str(string):
    """
    Compress a str to reduce size.
    """
    compress_string = zlib.compress(string)
    return compress_string


def divide_str(n_division, string, chunk_name='chunk'):
    """
    Divide a str in 'n_division' segments.
    """
    str_len = len(string)
    chunk_size = int(np.ceil(str_len / n_division))
    for ii in range(n_division):
        value_init = chunk_size * ii
        value_end = chunk_size * (ii + 1)
        chunked_str = string[value_init:value_end]
        f_name = '{}/{}{}.bin'.format(DIR_CHUNK, chunk_name, ii + 1)
        with open(f_name, "wb") as file:
            file.write(chunked_str)
    return 0
