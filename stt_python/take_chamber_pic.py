import aux_functions as fn
import subprocess
import sys
import time

try:
    ep, f1p = (11, 7)
    led1, led2, led3, led4 = (33, 37, 40, 38)
    fn.set_gpio_chamber(led1, led2, led3, led4, ep, f1p)

    print("--> STT: Taking picture of chamber")
    fn.select_camera(2, ep, f1p)

    current_time = time.strftime("%d%m%y_%H%M%S", time.localtime())
    pic_name = "/home/pi/chamber_pictures/{}.jpg".format(current_time)
    task = 'raspistill -t 1 -o {}'.format(pic_name)
    print('EXECUTING: ', task)

    fn.turn_on_led(led1, led2, led3, led4)
    process = subprocess.Popen(task, shell=True, stdout=subprocess.PIPE)
    process.wait()
    fn.turn_off_led(led1, led2, led3, led4)
    fn.clean_gpio()
    fn.reduce_img_size(pic_name)
    sys.exit(77)
except Exception as err:
    print("--> ERROR:", err)
    fn.clean_gpio()
    sys.exit(78)
