import aux_functions as fn
import sys

try:
    folder = sys.argv[1]
    img_name = sys.argv[2]
    n_div = int(sys.argv[3])

    if folder == 'stt':
        dir_folder = 'STT_pictures'
    elif folder == 'chamber':
        dir_folder = 'chamber_pictures'
    elif folder == 'flir':
        dir_folder = 'FLIR_pictures'
    else:
        err_msg = 'ERROR: Please introduce a valid folder!'
        raise ValueError(err_msg)

    img_dir = '/home/pi/{}/{}'.format(dir_folder, img_name)
    img_as_str = fn.img2str(img_dir)
    str_compressed = fn.compress_str(img_as_str)
    fn.divide_str(n_div, str_compressed)

    sys.exit(77)
except Exception as err:
    print("--> ERROR:", err)
    sys.exit(78)
