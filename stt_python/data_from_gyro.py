import angle_functions as angle_fn
import smbus
import sys

try:
    # Registros MPU6050 a utilizar
    PWR_MGMT_1 = 0x6B
    SMPLRT_DIV = 0x19
    CONFIG = 0x1A
    GYRO_CONFIG = 0x1B
    INT_ENABLE = 0x38
    GYRO_XOUT_H = 0x43
    GYRO_YOUT_H = 0x45
    GYRO_ZOUT_H = 0x47

    bus = smbus.SMBus(1)
    Device_Address = 0x68  # Direccion-MPU

    # sensivilidad-escalado
    sg1 = 131  # +-250 degree
    sg2 = 65.5  # +-500 degree
    sg3 = 32.8  # +-1000 degree
    sg4 = 16.4  # +-2000 degree

    global a

    a = sg1

    angle_fn.MPU_Init(bus, Device_Address, SMPLRT_DIV, PWR_MGMT_1, CONFIG, GYRO_CONFIG, INT_ENABLE)

    # Raw data from gyro
    gyro_x = angle_fn.read_raw_data(bus, Device_Address, GYRO_XOUT_H)
    gyro_y = angle_fn.read_raw_data(bus, Device_Address, GYRO_YOUT_H)
    gyro_z = angle_fn.read_raw_data(bus, Device_Address, GYRO_ZOUT_H)

    gx = (gyro_x / a)
    gy = (gyro_y / a)
    gz = (gyro_z / a)
    print(gx, gy, gz)

    with open("/home/pi/suchai-software-template/stt_data/gyro_data.txt", "w")as f:
        f.write("{} {} {}".format(gx, gy, gz))
    sys.exit(77)
except Exception as err:
    with open("/home/pi/suchai-software-template/stt_data/gyro_data.txt", "w")as f:
        f.write("0 0 0")
    sys.exit(78)
