import aux_functions as fn
import stt_functions
import sys

try:
    ep, f1p = [17, 4]
    fn.set_gpio(ep, f1p)

    init_time = int(sys.argv[1])
    final_time = int(sys.argv[2])
    time_step = int(sys.argv[3])
    n_stars = int(sys.argv[4])
    print("--> STT: Evaluating exposure time starting at {} ms, ending at {} ms, and time_step {} ms".format(init_time, final_time, time_step))
    fn.select_camera(1, ep, f1p)

    stt_functions.evaluate_exp_time(init_time, final_time, time_step, n_stars)
    fn.clean_gpio()
    sys.exit(77)
except Exception as err:
    print(err)
    # with open("/home/samuel/GitLab/suchai-software-template/stt_data/exp_time.txt", "w")as f:
    with open("/home/pi/suchai-software-template/stt_data/exp_time.txt", "w")as f:
        f.write("0 0")
    fn.clean_gpio()
    sys.exit(78)
