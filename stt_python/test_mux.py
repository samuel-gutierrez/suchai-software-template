import aux_functions as fn
import os
import subprocess
import sys
import time


try:
    ep, f1p = [17, 4]
    fn.set_gpio(ep, f1p)

    cam_nmbr = int(sys.argv[1])
    print("--> STT: Taking test picture with camera number {}".format(cam_nmbr))

    if cam_nmbr == 1:
        fn.select_camera(1, ep, f1p)
        cmd = "raspistill -n -t 1 -v -o /home/pi/suchai-software-template/capture_1.jpg"
        process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
        process.wait()
        time.sleep(1)
        if os.path.isfile("/home/pi/suchai-software-template/capture_1.jpg"):
            os.remove("/home/pi/suchai-software-template/capture_1.jpg")
        else:
            raise FileNotFoundError
        print('--> CAM 1 OK!')
        fn.clean_gpio()
        sys.exit(77)
    elif cam_nmbr == 2:
        fn.select_camera(2, ep, f1p)
        cmd = "raspistill -n -t 1 -v -o /home/pi/suchai-software-template/capture_2.jpg"
        process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
        process.wait()
        time.sleep(1)
        if os.path.isfile("/home/pi/suchai-software-template/capture_2.jpg"):
            os.remove("/home/pi/suchai-software-template/capture_2.jpg")
        else:
            raise FileNotFoundError
        print('--> CAM 2 OK!')
        fn.clean_gpio()
        sys.exit(78)
    else:
        print('---> Doing nothing! Please select camera <1> or <2>')
except Exception as err:
    print('---> ERROR: ', err)
    fn.clean_gpio()
    sys.exit(79)
